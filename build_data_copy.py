import healpy as hp
import os
import useful as us

nside_final = 512 #le nombre de pixels permettant une execution rapide des codes



for freq in us.FREQUENCIES:
    if int(freq)<99:
        for mode in us.MODE:
            for nside in us.NSIDE :
                if os.path.exists(us.DATAP +us.LFI_PATH.format(frequence=freq,nside=nside,mode=mode)):
                    
                    carte = hp.read_map(us.DATAP + us.LFI_PATH.format(frequence=freq,nside=nside,mode=mode))
                    lol = hp.pixelfunc.ud_grade(carte,nside_final)
                    hp.fitsfunc.write_map(us.WD+us.LFI_PATH_DEG.format(frequence=freq,nside=nside_final,mode=mode,p_nside=nside),lol)
    else :
        for mode in us.MODE:
            for nside in us.NSIDE :
                if os.path.exists(us.DATAP + us.LFI_PATH.format(frequence=freq,nside=nside,mode=mode)):
                    
                    carte = hp.read_map(us.DATAP + us.HFI_PATH.format(frequence=freq,nside=nside,mode=mode))
                    lol = hp.pixelfunc.ud_grade(carte,nside_final)
                    hp.fitsfunc.write_map(us.WD+us.HFI_PATH_DEG.format(frequence=freq,nside=nside_final,mode=mode,p_nside=nside),lol)
                    
                    
            
