#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 28 16:52:29 2019

@author: baudin
"""

import healpy as hp
import numpy as np
import matplotlib.pyplot as plt
from astropy import wcs

def make_WCS(longitude,latitude,npixel,resolution):
    w = wcs.WCS(naxis=2)
    w.wcs.crpix = [npixel/2,npixel/2]
    w.wcs.cdelt = np.array([-resolution,resolution])
    w.wcs.crval = [longitude,latitude]
    w.wcs.ctype = ["GLON-TAN","GLAT-TAN"]
    return w


def test():
    npixel=64
    longitude=6.76
    latitude=30.45
    resolution=1/60
    
    patch = np.zeros((npixel,npixel))
    w = make_WCS(longitude,latitude,npixel,resolution)

    y,x = np.indices(np.shape(patch))
    lon,lat = w.all_pix2world(x,y,0)

    carte = hp.read_map("/home/abeelen/Planck/maps/HFI_SkyMap_100_2048_R2.00_full.fits")
    #carte = hp.read_map("/home/abeelen/Planck/maps/HFI_SkyMap_353_2048_R2.00_full.fits")
    patch = carte[hp.ang2pix(hp.get_nside(carte),lon,lat,lonlat=True)]

    plt.imshow(patch,origin='lower')
    hp.mollview(carte,norm='hist')
    
#test() Test de la routine pour une seule carte
    
#################################################################################
#################################################################################  
    
def make_patch(longitude,latitude,npixel,resolution,carte):
    patch = np.zeros((npixel,npixel))
    w = make_WCS(longitude,latitude,npixel,resolution)
    
    y,x = np.indices(np.shape(patch))
    lon,lat = w.all_pix2world(x,y,0)
    
    patch = carte[hp.ang2pix(hp.get_nside(carte),lon,lat,lonlat=True)]
    
    return patch
    
def show_patch(patch):
    plt.figure()
    plt.imshow(patch,origin='lower')
    plt.show()

#show_patch(make_patch(6.76,30.45,64,1/60,hp.read_map("/home/abeelen/Planck/maps/HFI_SkyMap_100_2048_R2.00_full.fits")))