DATAP = "/home/abeelen/Planck/maps/"
WD = "/home/benkadou/projet_astro/data/"
COMMUN = "/home/p2_benkadoum_baudin/data/"

FREQUENCIES = ["217","100","143","353","545","857","030","044","070"]
MODE = ["halfmission-1","halfmission-2","full"]
NSIDE = ["1024","2048"]

HFI_PATH =  "HFI_SkyMap_{frequence}_{nside}_R2.00_{mode}.fits"
LFI_PATH =  "LFI_SkyMap_{frequence}_{nside}_R2.00_{mode}.fits"


HFI_PATH_DEG =  "HFI_SkyMap_{frequence}_{nside}_R2.00_{mode}_({p_nside}).fits"
LFI_PATH_DEG =  "LFI_SkyMap_{frequence}_{nside}_R2.00_{mode}_({p_nside}).fits"
