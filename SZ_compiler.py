#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  2 16:41:00 2019

@author: baudin
"""

import map_loader as ml
import patch_maker as pm
import math
import useful_copy as us
import numpy as np
import healpy as hp

# Paramètres à choisir
longitude = 6.76 #[deg]
latitude = 30.45 #[deg]
resolution = 1/60 #[deg/pixel]
npixel = 64

# Constantes
k = 1.38065e-23 #Constante de Boltzmann
h = 6.626e-34 #Constante de Planck
Tcmb = 2.73 #Température moyenne du corps noir CMB
#conv_Sz = [-0.24815, -0.35923, 5.152, 0.161098, 0.06918, 0.0380]
frequences = [545, 857]
# Variables
cartes = ml.load_maps()
patches = []
tSz_mixing_vector = np.zeros(len(frequences))


# Fonctions
def tSz_frequency_dependence(freq): #freq en GHz
    x = h*freq*1e9/k/Tcmb
    return x*(math.exp(x)+1)/(math.exp(x)-1)-4



# Création des patches
for carte in cartes:
    patches = pm.make_patch(longitude,latitude,npixel,resolution,carte)

# Création du mixing vector et de la matrice de covariance
for i in range(0,len(frequences)):
    tSz_mixing_vector[i]= tSz_frequency_dependence(frequences[i])
    
covariance = np.cov(patches)

print(np.shape(covariance))
# Estimation du signal tSz
#signal_tSz = np.dot(np.dot(tSz_mixing_vector.T,np.linalg.inv(covariance)),patches) / np.dot(np.dot(tSz_mixing_vector.T,np.linalg.inv(covariance)),tSz_mixing_vector)

#hp.mollview(signal_tSz,norm='hist')

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    