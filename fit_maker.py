#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 18 17:28:35 2019

@author: baudin
"""

import patch_maker as pm
from astropy import wcs
import astropy.io.fits  

def make_fit(name,longitude,latitude,npixel,resolution,carte):
    patch = pm.make_patch(longitude,latitude,npixel,resolution,carte)
    w = pm.make_WCS(longitude,latitude,npixel,resolution)
    header = wcs.to_header(w)
    astropy.io.fits.writeto(name,patch,header,overwrite=True)
    
    
    

