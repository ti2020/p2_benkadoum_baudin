#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 18 14:57:39 2019

@author: baudin
"""

# Routine réalisant une moyenne en fréquence d'un même patch découpé dans le ciel

import patch_maker as pm
import fit_maker as fm
import useful_copy as us
import matplotlib.pyplot as plt
import healpy as hp
import os

MODE = "full"

longitude = 6.76 #[deg]
latitude = 30.45 #[deg]
#taille = 10 #[deg]
resolution = 1/60 #[deg/pixel]
npixel = 64


patches = []
titles = []

for freq in us.FREQUENCIES:
    for nside in us.NSIDE :
        if int(freq)<99:
            if os.path.exists(us.DATAP + us.LFI_PATH.format(frequence=freq,nside=nside,mode=MODE)):
                print("reading " + us.DATAP + us.LFI_PATH.format(frequence=freq,nside=nside,mode=MODE))
                patches.append(pm.make_patch(longitude,latitude,npixel,resolution,hp.read_map(us.DATAP + us.LFI_PATH.format(frequence=freq,nside=nside,mode=MODE))))
                print(" ")
                
        else:
            if os.path.exists(us.DATAP + us.HFI_PATH.format(frequence=freq,nside=nside,mode=MODE)):
                print("reading " + us.DATAP + us.LFI_PATH.format(frequence=freq,nside=nside,mode=MODE))
                patches.append(pm.make_patch(longitude,latitude,npixel,resolution,hp.read_map(us.DATAP + us.HFI_PATH.format(frequence=freq,nside=nside,mode=MODE))))
                print(" ")
                
        titles.append( freq + " GHz, " + nside + " nside" )
        

    
for i in range(0,len(patches)):
    plt.subplot(2,5,i+1)
    plt.imshow(patches[i],origin='lower')
    plt.title(titles[i])
