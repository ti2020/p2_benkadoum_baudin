#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 28 16:52:29 2019

@author: baudin
"""

import healpy as hp
import useful_copy as us
import numpy as np
from astropy import wcs
from astropy.io import fits

def make_WCS(latitude,longitude,lengthX,lengthY,resolution):
    w = wcs.WCS(naxis=2)
    w.wcs.crpix = [lengthX*resolution/2,lengthY*resolution/2]
    w.wcs.crval = [latitude,longitude]
    w.wcs.ctype = ["RA---TAN","DEC---TAN"]
    return w


    
    
    
    
    
    
    
    
    
    
    
    
    
carte = hp.read_map(us.DATAP + "LFI_SkyMap_044_1024_R2.00_full.fits")